import subprocess
import re
from string import *




# Variables
entryList = []			# List of entry in a format of dictionary
index = 0				# Unique number of entry supporting hosts with a same name
parent = [0] * 100		# Unique number of the parent (sub) menu
prevEntryIsMenu = 0		# Flag to identify a particular entry as (sub) menu
thisLevel = 0			# Current depth in the targets structure
prevLevel = 0			# Previous depth in the targets structure



####
#
# Read Targets file and extract the parent menu of target hosts
#

# infile = 'Targets'
infile = "/etc/smokeping/config.d/Targets"

for inline in file(infile):
	
	if re.match(r'\+', inline):

		entry = {"index":index, "name":strip(re.split('\ ', inline)[1]), "type":"menu"}		
		thisLevel = inline.count('+')
		#		print str(index) + " " + str(thisLevel),
		index += 1
		
		if (thisLevel == 1):
			entry['parent'] = -1
			parent[thisLevel] = -1
			prevLevel = thisLevel
			prevEntryIsMenu = 1

			
		elif (thisLevel > prevLevel):
			parent[prevLevel] = entryList[-1]['index']

			entry['parent'] = parent[thisLevel-1]
			prevLevel = thisLevel

		elif (thisLevel == prevLevel):
			if (prevEntryIsMenu == 1):
				parent[prevLevel] = entryList[-1]['index']	

			entry['parent'] = parent[thisLevel - 1]
			prevLevel = thisLevel
			
		elif (thisLevel < prevLevel):
			if (prevEntryIsMenu == 1):
				parent[prevLevel] = entryList[-1]['index']
			entry['parent'] = parent[thisLevel -1]
			prevLevel = thisLevel
		
		entryList.append(entry)
		continue
	
	if re.match(r'host', inline):
		entryList[-1]['type'] = "host"
		prevEntryIsMenu = 0



####
#
# Extract the reverse path of each target
# and write the result to a temprary file
#
tempFile = 'TemporaryPaths'
tfp = open(tempFile, 'w')

# Recursive lookup of reverse path
def findPath(i):
	if (entryList[i]['parent'] == -1):
		return
	else:
		for j in range(len(entryList)): 
			if (entryList[j]['index'] == entryList[i]['parent']):
				tfp.write(entryList[j]['name'] + " ")
				findPath(j)

# Examine all hosts.  Menu must be skipped.
for i in range(len(entryList)):
	if (entryList[i]['type'] == "host"):
		tfp.write(entryList[i]['name'] + " ")
		findPath(i)
		tfp.write("\n")
	else:
		continue

tfp.close()

####
#
# Make up the rrdtool command.  Very long one.
#
c = 0
cmd = 'rrdtool xport --start now-3h --end now --maxrows 10 '

for tempLine in file(tempFile):
	words = re.split('\ ', tempLine)
#	print words
	finalPath = ""
        urlForDevice = ""
	
	# Skip "Curl"
	if (strip(words[len(words) - 2]) == "Curl"):
		continue
	
	for i in range(len(words)):
		finalPath += strip(words[len(words) - 2 - i])
		urlForDevice += strip(words[len(words) - 2 - i])
		if (len(words) - 2 - i == 0):
			break
		finalPath += "/"
		urlForDevice += "."


#	print finalPath

	c += 1
	p1 = 'DEF:d' + str(c) + '=/var/lib/smokeping/' + finalPath + '.rrd:ping1:AVERAGE '
	p2 = 'CDEF:c' + str(c) + '=d' + str(c) + ',100,*,20,/ '
       	if (c == 1):
		p3 = 'XPORT:c' + str(c) + ':"' + words[0] + '" > /var/www/RTT.xml '
	else:
		p3 = 'XPORT:c' + str(c) + ':"' + words[0] + '" >> /var/www/RTT.xml '
	cmd = cmd + p1 + p2 + p3

print cmd

####
#
# Execute rrdtool command
#
subprocess.call(cmd, shell=True)
