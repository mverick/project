# README #

SMOKEPING WRAPPER

### INTRODUCTION ###

* It's purpose is to visualize the LOSS RATES and RTT values obtained from the SmokePing tool regarding different network devices.


### Configuration/Requirements ###

* SmokePing server hosted on system.
* Python installed on the web server and the SmokePing server.


### Installation ###

* Host  “SmokePingWrapper” on a web server.
* Run the getxml.sh file.
* Run generateRTT.sh file

### Contact ###

* Ankit Rathor
* Pravi Malviya
* Utpal Bora